#!/usr/bin/python
# -*- coding: UTF-8 -*-
'''
Created on 1 aout 2012

@author: maxisoft
'''

import random
import pickle
import zlib
import hashlib
import os
import sys

# Globals

WPEEXE = '' # fichier exe en string
WPEDLL = '' # fichier dll en string

DLL_NAME = '' # WpeSpy.dll initialement
EXE_NAME = ''


#----------------------------------------------------------------------
def randomStr(n=random.randint(5, 12)):
    """Gen random str"""
    import string
    return ''.join([x for x in random.sample(string.ascii_letters, n)])


#---------------------------------------------------------------------- 
def md5Checksum(filePath):
    """http://www.joelverhagen.com/blog/2011/02/md5-hash-of-file-in-python/"""
    fh = open(filePath, 'rb')
    m = hashlib.md5()
    c = 0
    while True:
        data = fh.read(8192)
        if not data:
            break
        m.update(data)
        c = c + 1
    return m.hexdigest()


def readPickleData():
    '''Lit les données stocker dans data.pkl'''
    with open('data.pkl', 'rb') as pkl_file:
        return map(zlib.decompress, pickle.load(pkl_file))

def modFileSign(s):
    '''Change le contenu de fin du fichier.
    Implique changement de la signature du fichier.

    Retourne le contenu modifié.
    '''
    return s + randomStr() #...

def changeDllName(name=randomStr(6)):
    '''Modifie le fichier exe et renomme la dll.
    Nom aleatoire si pas d'argument.'''
    global WPEEXE
    global DLL_NAME
    
    WPEEXE = WPEEXE.replace('DLLmod', name)
    DLL_NAME = name
    
    
    
    
    
if __name__ == '__main__':
    WPEEXE, WPEDLL = map(modFileSign, readPickleData())
    
    print '''Entrer le nom de dll souhaiter (faire entre pour un nom aleatoire) ...'''
    DLL_NAME = str(raw_input()).capitalize().replace('.dll', '').strip()
    
    while len(DLL_NAME) != 6 and DLL_NAME is not '':
        DLL_NAME = ''
        print 'Erreur, le nom doit comporter 6 caracteres ... \nRerentrer un nom'
        DLL_NAME = str(raw_input()).capitalize().replace('.dll', '').strip()
        
    if not DLL_NAME: #non def
        changeDllName()
    else:
        changeDllName(DLL_NAME)
    
    print '''Entrer le nom du fichier EXE souhaiter (faire entre pour un nom aleatoire) ...'''
    EXE_NAME = str(raw_input()).capitalize().replace('.dll', '').strip()
    if not EXE_NAME:
        EXE_NAME = randomStr()
    
    
    #Add ext
    EXE_NAME += '.exe'
    DLL_NAME += '.dll'
    
    print '\nDebut de la generation des fichiers.'
    
    #changement dir
    try:
        os.chdir(os.path.dirname(sys.argv[0]))
    except:
        pass
    
    try:
        os.mkdir('Build')
    except:
        pass
    
    os.chdir('Build')
    
    #Save file
    with open(EXE_NAME, 'wb') as wpefile:
        wpefile.write(WPEEXE)
    with open(DLL_NAME, 'wb') as wpefile:
        wpefile.write(WPEDLL)
        
        
    print '\n Fin de la generation'
    
    
    
    #MD5 Log
    strmd5log = md5Checksum(EXE_NAME) + ' *' + EXE_NAME + \
                '\n' + md5Checksum(DLL_NAME) + ' *' + DLL_NAME
                
    with open('log.md5', 'w') as md5log:
        md5log.write(strmd5log)
    
    
    
    
        
    
    
    
    
    
    
    






